#Construir un programa que solicite un número entero no negativo al usuario, escribiendo el mensaje "Dame un número entero no negativo: "
num=int(input("Dame un número entero no negativo: "))
if num<0:
     quit()
#y que como respuesta escriba en una línea el mensaje "Números primos iguales o menores: "
else:

# a continuaciòn, en la misma línea, la lista de los números primos menores o iguales que él, separados por espacios.
# Para calcular los números primos, utiliza la definición: "un número entero es primo si sólo es divisible por 1 y por él mismo".
    for x in range (2,num+1):
        crece=2
        primo=True
        while primo and crece<x:
            if x%crece==0:
                primo=False
            else:
                crece +=1
        if primo:
            print(" Números primos iguales o menores: ", x, end="")